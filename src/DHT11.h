/*
 *  DHT11 pin is PG5 (none AF functions)
 */
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_tim.h"

#ifndef DHT11_H_
#define DHT11_H_

//errors coding
enum DHT11_ERRORS{
	DHT11_OK,
	DHT11_CRC_ERROR,
	DHT11_NO_RESPONSE,
	DHT11_TIME_OUT,
	DHT11_DECODE_ERROR
};
/**  struct which contains result 
  *  of temperature humidity
  *  and possible error code 
  */
typedef struct DHT11Result{
	float temperature;
	float humidity;
	int ERROR_CODE;
}DHT11Result;

/**
  * @brief  measuring temperature and humidity using DHT11
  * @note   also invokes setup function for  GPIO and timers
  * @param  None
  * @retval DHT11Result: struct with results (and error codes)
  */
DHT11Result DHT11_read();

#endif /* DHT11_H_ */
