#ifndef BUTTON_H_
#define BUTTON_H_
#include "stm32f429i_discovery_lcd.h"
#include <string.h>

// state of Button
enum ButtonState{
	UNCLICKED,
	CLICKED
};
/**
  * Button struct contains coordinates
  * @x
  * @y
  * and label of Button
  * @string
  */
struct Button{
	uint16_t x;
	uint16_t y;
	const char * string; 
};
typedef  struct Button Button;


/**
  * @brief  Drawing button on LCD
  * @param  toDraw: pointer to a Button structure that contains
  *         information about position and  label of given Button
  * @retval None
  */
void ButtonDraw(Button * toDraw);

/**
  * @brief  Checks if LCD was touched atco area of given Button
  * @param  x: 	     x coordinate of Button
  * @param  y:       y coordinate of Button
  * @param  toCheck: Button witch is being check 
  *         the configuration information for the CRYP Initialization Vectors(IV).
  * @retval CLICKED:   if Button area contains given coordinates of Button
  * 	    UNCLICKED: if it does not contains given coordinates of Button	
  */
int ButtonCheckIfPressed(uint16_t x,uint16_t y,Button * toCheck);

//struktura etykiety
struct Label{
	uint16_t x;
	uint16_t y;
	const char * string;
};

typedef  struct Label Label;

/**
  * @brief  Drawing Label on LCD
  * @param  toDraw: pointer to a Label structure that contains
  *         information about position and  label of given Label
  * @retval None
  */
void LabelDraw(Label * toDraw);

/**
  * @brief  Converting given data into array of chars
  * @note   and making new Label object
  * 	    by nvoking @LabelDraw()
  * @param  x:        x coordinate of Label
  * @param  y:        y coordinate of Label
  * @param  data:     data that will be displayed on Label
  * @param  precision: decimal places that @data will have after conversion to array of chars
  * @retval None
  */
void DrawFloat(uint16_t x,uint16_t y,float data,int precision);
#endif /* BUTTON_H_ */
