/*
 * ConfigurationPanel.h
 *
 *  Created on: Jun 2, 2016
 *      Author: krzys
 */


#ifndef CONFIGURATIONPANEL_H_
#define CONFIGURATIONPANEL_H_

#include "Button.h"
#include "stm32f429i_discovery_lcd.h"
#include "stm32f429i_discovery_ioe.h"
#include "Chart.h"
#include "core_cm4.h"
#include "Results.h"

void ConfigurationPanel();

#endif /* CONFIGURATIONPANEL_H_ */
