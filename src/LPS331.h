#ifndef LPS331_term
#define LPS331_term

#include "stm32f4xx_i2c.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

#define LPS331_adress 0b1011101
#define LPS331_PRESS_POUT_XL_REH 0x28
#define LPS331_PRESS_LOW 0x29
#define LPS331_PRESS_HIGH 0x2A
#define LPS331_CTRL_REG1 0x20

/**
 * struct representing result of measure
 * and possible error code
 */
struct LPS331Result{
	int ERROR_CODE;
	float pressure;
};

typedef struct LPS331Result LPS331Result;

/**
  * @brief  setup  I2C3 registers  
  * @param  none.
  * @retval none.
  */
void LPS331_INIT();

/**
  * @brief  measuring pressure using LPS331
  * @param  none.
  * @retval LPS331Result: struct with results (and error codes)
  */
LPS331Result LPS331_readPressure();

#endif
