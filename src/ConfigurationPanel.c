/*
 * ConfigurationPanel.c
 *
 *  Created on: Jun 2, 2016
 *      Author: krzys
 */
#include "ConfigurationPanel.h"
void ConfigurationPanel(){
	TP_STATE state;
	Label refreshLabel={5,10,"Refresh time[s]: "};
	Button plusButton={20,30,  "     +     "};
	Button minusButton={20,70, "     -     "};
	Button returnButton={5,250,"    Return    "};

	uint8_t updateRequest=1;

	while(1){
		__disable_irq();

		state=*IOE_TP_GetState();

		if (state.TouchDetected == 128 ) {
			if(ButtonCheckIfPressed(state.X,state.Y,&returnButton)==CLICKED){
				return;
			}
			else if(ButtonCheckIfPressed(state.X,state.Y,&plusButton)==CLICKED){
				if((results.MEASURE_INTERVAL_MS/1000)<1000){
					results.MEASURE_INTERVAL_MS=results.MEASURE_INTERVAL_MS+1000;
				}
				updateRequest=1;
			}
			else if(ButtonCheckIfPressed(state.X,state.Y,&minusButton)==CLICKED){
				if((results.MEASURE_INTERVAL_MS/1000)>1){
					results.MEASURE_INTERVAL_MS=results.MEASURE_INTERVAL_MS-1000;
				}
				updateRequest=1;
			}
			state.TouchDetected=0;
		}


		if(updateRequest){
			LCD_SetColors(0x0000,0xffff);
			LCD_DrawFullRect(0,0,240,320);

			LabelDraw(&refreshLabel);
			ButtonDraw(&plusButton);
			ButtonDraw(&minusButton);
			ButtonDraw(&returnButton);

			char tmp[4];
			intToStr(results.MEASURE_INTERVAL_MS/1000,tmp,4);
			Label timeLabel={150,10,tmp};
			LabelDraw(&timeLabel);
			updateRequest=0;

			delayMSC(50);

		}
		__enable_irq();
		delayMSC(100);
	}



}
