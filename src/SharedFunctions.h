#ifndef SHAREDFUNCTIONS_H_
#define SHAREDFUNCTIONS_H_

#include "stm32f4xx_gpio.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_i2c.h"
#include "DHT11.h"
#include "ds18b20.h"
#include "LPS331.h"

static uint8_t I2C3_CONFIGURED=0; // flag for I2C3

/**
  * @brief  enable and setup timer 2 clock.
  * @param  None
  * @retval None
  */
void InitTimerTIM2ToCountInUc();

/**
  * @brief  initiate I2C3 and GPIO and corresponding clocks
  * @param  None
  * @retval None
  */
void InitI2C3();

/**
  * @brief  wait given period of time given in ms
  * @param  ms: given time in Millisecond
  * @retval None
  */
void delayMSC(int ms);
#endif /* SHAREDFUNCTIONS_H_ */
