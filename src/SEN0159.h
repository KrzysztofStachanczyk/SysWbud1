#ifndef SEN0159_H_
#define SEN0159_H_

#include "stm32f4xx_gpio.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_adc.h"

/**
  * @brief  setup  GPIO and ADC
  * @param  none.
  * @retval none.
  */
void SEN0159init();


/**
  * @brief  measuring concentration of CO2 using SEN0159
  * @param  none.
  * @retval result of measure
  */
int SEN0159Result();
#endif /* SEN0159_H_ */
