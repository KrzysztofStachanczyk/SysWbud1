#ifndef CHART_H_
#define CHART_H_
#include "stm32f429i_discovery_lcd.h"
#include "Button.h"
/**
  * @brief  Drawing chart and Label on LCD
  * @note   coordinates, width and high
  *         are needed to draw a rectangle 
  *         on which it is drawn chart
  * @param  x: x coordinate 
  * @param  y: y coordinate
  * @param  w: width ( for scale )
  * @param  h: high (for scale)
  * @param  data: array of data to display on chart
  * @param  dataSize number of data elemets
  * @retval None
  */
void DrawChart(uint16_t x,uint16_t y,uint16_t w,uint16_t h,float * data,uint16_t dataSize);

#endif /* CHART_H_ */
