#ifndef CHARTSMENU_H_
#define CHARTSMENU_H_
#include "stm32f429i_discovery_ioe.h"
#include "Button.h"
#include "Results.h"
#include "ChartPanel.h"
#include "SharedFunctions.h"
#include "core_cm4.h"

/**
  * @brief  display menu with options
  *         that allow to disaply charts
  * @param  None
  * @retval None
  */
void ChartsMenu();

#endif /* CHARTSMENU_H_ */
