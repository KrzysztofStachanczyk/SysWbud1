#ifndef CHARTPANEL_H_
#define CHARTPANEL_H_
#include "Button.h"
#include "stm32f429i_discovery_lcd.h"
#include "stm32f429i_discovery_ioe.h"
#include "Chart.h"
#include "core_cm4.h"
#include "Results.h"

/**
  * @brief  LCD display for charts
  * @param  chartName: display name of the chart
  * @param  data: array: of data to display on chart
  * @param  sizeOfData: number of data elemets
  * @retval None
  */
void ChartPanel(char * chartName,float * pointerToDataArray,int sizeOfData);


#endif /* CHARTPANEL_H_ */
