#ifndef FLOATCONVERTER_H_
#define FLOATCONVERTER_H_

#include<stdio.h>
#include<math.h>

/**
  * @brief  reverses a string 
  * @param  str: pointer to char array which is being reversed
  * @param  len: number of elements in array
  * @retval none.  
  */
void reverse(char *str, int len);


 
/**
  * @brief  Converts a given integer to array of chars
  * @param  x:   number to convert 
  * @param  str: array of char as result
  * @param  d:   number of elements in array
  * @retval number of digit in number  
  */
int intToStr(int x, char str[], int d);

/**
  * @brief  Converts a floating point number to char array.
  * @param  n:            number to convert 
  * @param  str:          array of char as result
  * @param  afterpoint:   precision
  * @retval none. 
  */
void ftoa(float n, char *res, int afterpoint);

#endif /* FLOATCONVERTER_H_ */
