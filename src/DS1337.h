#ifndef DS1337_LOCKER
#define DS1337_LOCKER

#include "stm32f4xx_gpio.h"
#include "stm32f4xx.h"
#include "stm32f4xx_i2c.h"
#include "stm32f4xx_rcc.h"
#include "SharedFunctions.h"

#define DS1337_BEGIN_OF_TIME_REGISTER 0x00
#define DS1337_BEGIN_OF_ALARM1_REGISTER 0x07
#define DS1337_BEGIN_OF_ALARM2_REGISTER 0x0B
#define DS1337_CONTROL_REGISTER 0x0E
#define DS1337_STATUS_REGISTER 0x0F

#define DS1337_ADRESS 0b1101000
//error code
struct DS1337_STATUS_STRUCT{
	int ERROR_CODE;
}DS1337_STATUS;

/**
 * error codes
 */
enum DS1337_ERRORS{
	DS1337_OK,
	DS1337_TIME_OUT
};
/**
 * time represent struct
 */
struct DS1337_TIME_STRUCT{
	uint8_t seconds;
	uint8_t minutes;
	uint8_t hour;
	uint8_t day;
	uint8_t date;
	uint8_t month;
	uint8_t decade;
}DS1337_TIME;

/**
  * @brief  setup I2C3 if I2C3 is not configured
  * @param  none.
  * @retval none.
  */
void ds1337_INIT();

/**
  * @brief  configuring registers and sent data to selected register
  * @param  adress: register ID adres, I2C Send Data there
  * @param  value:  value to send
  * @retval none.
  */
void ds1337_writeRecordToRegister(uint8_t adress,uint8_t value);

/**
  * @brief  configuring registers and sent data to selected register
  * @note   overload function 
  * @param  beginAdress: begin ID of writing register 
  * @param  array:       array with data
  * @param  size:        number of elements in @array
  * @retval none.
  */
void ds1337_writeRecordsToRegisters(uint8_t beginAdress,uint8_t * array,uint8_t size);

/**
  * @brief  configuring registers and read data from selected register
  * @param  adress:      reading register ID
  * @param  ptrToBuffer: buffer for data
  * @retval none.
  */
void ds1337_readRecordsFromRegister(uint8_t adress,uint8_t * ptrToBuffer);

/**
  * @brief  configuring registers and read data from selected register
  * @note   overload function
  * @param  array: array with read data 
  * @param  size: number of elements in @array
  * @retval none.
  */
void ds1337_readRecordsFromRegisters(uint8_t beginAdress,uint8_t * array,uint8_t size);

/**
  * @brief  Convert integers to BDC format using by RTC 
  * 	    and save it to RTC
  * @param  none.
  * @retval none.
  */
void ds1337_writeTimeStructure();

#endif
