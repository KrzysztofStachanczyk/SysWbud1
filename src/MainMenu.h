#include "stm32f429i_discovery_lcd.h"
#include "Button.h"
#include "stm32f429i_discovery_ioe.h"
#include "Chart.h"
#include "ChartsMenu.h"
#include "ConfigurationPanel.h"
#include "Results.h"
#include "core_cm4.h"

#ifndef MAINMENU_H_
#define MAINMENU_H_

/**
  * @brief  initiate LCD
  * @param  none.
  * @retval none.
  */
void LCDinit(void);

/**
  * @brief  Display Main menu with buttons
  * @param  none.
  * @retval none.
  */
void MainMenu();

#endif /* MAINMENU_H_ */
