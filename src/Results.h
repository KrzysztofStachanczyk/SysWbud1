#ifndef RESULT_MARK
#define RESULT_MARK

#include "DHT11.h"
#include "LPS331.h"
#include "ds18b20.h"
#include "stm32f4xx_dma.h"
#define BUFFER_SIZE 220

/**
 * struct with collected data and measurements properties
 */

struct{
	uint32_t sampleNumber;
	uint32_t MEASURE_INTERVAL_MS;
	volatile uint8_t changed;
	uint8_t sensorReadingTime;
	DHT11Result temperatureAndHumidity;
	LPS331Result pressure;
	ds18b20Result externalTemperature;

	// 0-temperature 1-humidity 2-pressure 3-external

	float temperaturesHistory[BUFFER_SIZE];
	float pressureHistory[BUFFER_SIZE];
	float extTemperaturesHistory[BUFFER_SIZE];
	float humidityHistory[BUFFER_SIZE];

	//float pastValues[4][BUFFER_SIZE];
}results;

/**
  * @brief  saving current measurements
  * @param  none.
  * @retval none.
  */
void collectDataFromSensors();

/**
  * @brief  Shifts data in measurements History arrays using DMA
  * @param  none.
  * @retval none.
  */
void DMArecordPushInit();

/**
  * @brief  setup DMA for shifting operation
  * @param  none.
  * @retval none.
  */
void pushRecord();








#endif
